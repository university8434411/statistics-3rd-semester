"""
A simple script for handling all the variables and tables related to basic frequency tables.

It implements a class and basic oop in python so everyone feels like in home.

You are free to use it for your homework, and if you want to contribute it'll be amazing. 

@author: José Manuel Pérez Rodríguez --- josemanuelpr@ufps.edu.co
"""

from collections import Counter


class FrequencyTable:
    """
    This class contains a description of the table and a list of frequencies, and can give
    the other necessary fields of a basic frequency table seen in the statistics course. 
    """

    def __init__(self, description=""):
        self.description = description
        self.sample = 0
        self.data = []
        self.frequencies = []
        self.parameters = []

    def is_data_empty(self):
        """
        Checks if the list of frequencies is empty for the current object.
        :return: A boolean value representing the emptiness of the frequencies.
        """
        return len(self.data) == 0

    def get_data(self):
        """

        :return:
        """
        return self.data

    def get_frequencies(self):
        return self.frequencies

    def get_parameters(self):
        """
        Returns the parameters.
        The frequencies must have been defined previously.
        """
        if self.is_data_empty():
            self.raise_empty_data_error()

        return self.parameters

    def get_angles(self):
        """
        Generates all the values for the angles column.
        returns a list with the generated values.
        Doesn't need any prior attribute besides the frequencies.
        """
        if self.is_data_empty():
            self.raise_empty_data_error()

        return [round((freq / self.sample) * 360, 2) for freq in self.frequencies]

    def get_absolute_frequencies(self):
        """
        Generates all the values for the absolute frequencies column.
        returns a list with the generated values.
        Doesn't need any prior attribute besides the frequencies.
        """
        if self.is_data_empty():
            self.raise_empty_data_error()

        absolute_frequencies = []
        frequencies_sum = 0
        for i in range(len(self.frequencies)):
            frequencies_sum += self.frequencies[i]
            absolute_frequencies.append(frequencies_sum)

        return absolute_frequencies

    def get_percentages(self):
        """
        Generates all the values for the percentages column.
        Saves it in the object and also returns a list with the generated values.
        Doesn't need any prior attribute besides the frequencies.
        """
        if self.is_data_empty():
            self.raise_empty_data_error()

        return [round((freq / self.sample) * 100, 2) for freq in self.frequencies]

    def get_relative_frequencies(self):
        """
        Generates all the values for the relative frequencies column.
        Saves it in the object and also returns a list with the generated values.
        Doesn't need any prior attribute besides the frequencies.
        """
        if self.is_data_empty():
            self.raise_empty_data_error()

        return [round((freq / self.sample), 2) for freq in self.frequencies]

    def set_data(self, arr):
        """
        Saves a given array of frequencies.
        Also sets the value of sample and parameters based on the frequencies.
        """
        if not isinstance(arr, list):
            raise TypeError("The parameter is not a list")

        data_counter = Counter(arr)

        self.data = arr
        self.sample = len(arr)
        self.frequencies = sorted(list(data_counter.values()))
        self.parameters = sorted(list(data_counter.keys()))

    def set_description(self, text):
        """
        Given a string, saves it in the description.
        """
        if not isinstance(text, str):
            raise TypeError("The parameter is not a string.")
        else:
            self.description = text

    @staticmethod
    def raise_empty_data_error():
        exception_message = """
            The data list is empty. First call the method add_data(arr)
            in order to execute this method.
            """
        raise ValueError(exception_message)


"""
A simple example of how to use it
"""
if __name__ == "__main__":
    table = FrequencyTable()
    data = [32, 33, 33, 32, 32, 32, 32, 30,
            28, 31, 32, 33, 34, 30, 32, 32,
            29, 32, 31, 31, 30, 31, 31, 32,
            26, 32, 31, 32, 33]  # temperatures

    table.set_data(data)
    angles = table.get_angles()


    print(table.get_parameters())
    print(table.get_frequencies())
    print(table.get_absolute_frequencies())
    print(table.get_relative_frequencies())
    print(table.get_percentages())
    print(table.get_angles())
