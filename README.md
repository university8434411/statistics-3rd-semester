# Statistics 3rd semester

## Name
Ooba-Boonga

## Description
Just a little side project in order to realize most of the homework for the statistics course.
Also it's just to look and learn some python technologies.

What will be implemented (once i have the time to implement it) is: tkinter for a basic gui, 
Mathplotlib for graphs and if it turns too complicated to do some calculations, numpy.

## Installation
Any IDE for python will work, but if you want to use the same as the one i'm currently using,
it's Pycharm community edition.
The python version that's being implemented is 3.10, and to install all the dependencies just
run 
```
pip install tk mathplotlib numpy
```
on your terminal (you must have pip installed beforehand), works in Linux, Mac or Windows.

## Usage
I just created the project and its working via the terminal, in the following weeks i'll
be adding a very simple gui with tkinter so it isn't too gross to use.

for now it only works with a frequency table, that is handled by a FrequencyTable class.
I haven't docummented it because... i just started the project lol.


## Roadmap
It's defenitly not something big, just a simple program to do the boring part of the statistics
course, i mean who wants to fill all those tables and graphs.

## Contributing


## Authors and acknowledgment
Just the creator of this repo, if you'd like to contribute... my way to say thanks is to just
put your name in this section ;)

## License
huuummmm, you can clone it and use it, i don't think is necessary a MIT license or something for
now, feel free to make use of this if you desire.

## Project status
Created.
